/*
 * Echo adapter for testing
 */

var logger = require('logger').create(module);

function list(params, feedConfig, dataSourceConfig, next) {
  next({operation: 'list', params: params, feedConfig: feedConfig, dataSourceConfig: dataSourceConfig});
};

function get(params, feedConfig, dataSourceConfig, next) {
  next({operation: 'get', params: params, feedConfig: feedConfig, dataSourceConfig: dataSourceConfig});
};

function insert(params, entry, feedConfig, dataSourceConfig, next) {
  next({operation: 'insert', params: params, feedConfig: feedConfig, dataSourceConfig: dataSourceConfig});
};

function update(params, entry, feedConfig, dataSourceConfig, next) {
  next({operation: 'update', params: params, feedConfig: feedConfig, dataSourceConfig: dataSourceConfig});
};

function remove(params, feedConfig, dataSourceConfig, next) {
  next({operation: 'remove', params: params, feedConfig: feedConfig, dataSourceConfig: dataSourceConfig});
};

exports.list = list;
exports.get = get;
exports.insert = insert;
exports.update = update;
exports.remove = remove;
