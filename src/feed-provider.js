/**
 * All functions with a "handle" prefix are request and response handlers in
 * the style of Connect.  All functions with a "makeHandler" prefix create and
 * return such a handler.
 */

var composition = require('./composition');
var fs = require('fs');
var logger = require('./logger').create(module);
var querystring = require('querystring');
var url = require('url');

var CONF_DIR = process.env.NODE_FEEDSERVER_CONF_DIR;
var ENV = global.INSTANCE || process.env.NODE_FEEDSERVER_ENV;
var GROUP_PREFIX = 'g:';

/**
 * Adds runtime environment (e.g. DEV, TST, STG and PRD) to a name.  For
 * example, if ENV is null, then withInstance('cedartc-db') is 'cedartc-db.
 * If ENV is 'DEV', then withInstance('cedartc-db') is 'cedartc-db.DEV'.
 */
function withInstance(name) {
  return ENV ? name + '.' + ENV : name;
};

function extend(object0, object1) {
  object0.__proto__ = object1;
  return object0;
};

function getConfigFilePath() {
  var path = [CONF_DIR ? CONF_DIR : 'conf'];
  for (var i = 0; i < arguments.length - 2; i++) {
    if (arguments[i]) {
      path.push(arguments[i]);
    }
  }
  return path.join('/');
};

var configFileCache = {};

/**
 * Reads a JSON file and returns parsed object
 * @param arguments Argument list of path segments and second last is next()
 * and last handleError
 */
function getConfig() {
  var next = arguments[arguments.length - 2];
  var nextError = arguments[arguments.length - 1];
  var configFilePath = getConfigFilePath.apply(this, arguments);
  var configContent = configFileCache[configFilePath];
  if (configContent) {
    next(JSON.parse(configContent));
  } else {
    fs.readFile(configFilePath, 'utf8', function(error, configContent) {
      logger.log('getConfig() path=' + configFilePath);
      error ? (nextError ? nextError(error) :
          console.log(error)) :
          configFileCache[configFilePath] = configContent,
          next(JSON.parse(configContent));
    });
  }
};

function handleGetFeedConfig(request, response, next) {
  getConfig('feeds', request.params.namespace, 'feed',
      request.params.ownerId ? 'user' : null,
      request.params.feedId, function(feedConfig) {
    request.config.feed = feedConfig;
    next(request, response); 
  }, handleError(response));
};

function handleGetDataSourceConfig(request, response, next) {
  getDataSourceConfig_(request.params.namespace, request.config.feed.datasource,
      request.config, function() {
    next(request, response);
  }, handleError(response));
};

function getDataSourceConfig_(namespace, dataSourceId, config, next, nextError) {
  // read datasource config with instance first
  getConfig('feeds', namespace, 'datasource', withInstance(dataSourceId),
      function(dataSourceConfig) {
    config.dataSource = dataSourceConfig;
    next();
  }, function(error) {
    // read datasource config without instance (see if default is there)
    getConfig('feeds', namespace, 'datasource', dataSourceId,
        function(dataSourceConfig) {
      config.dataSource = dataSourceConfig;
      next();
    }, nextError);
  });
};

function handleGetAdapterConfig(request, response, next) {
  getAdapterConfig_(request.params.namespace, request.config.dataSource.adapter,
      request.config, function() {
    next(request, response);
  }, handleError(response));
};

function getAdapterConfig_(namespace, adapterId, config, next, nextError) {
  getConfig('feeds', namespace, 'adapter', adapterId, function(adapterConfig) {
    config.adapter = adapterConfig;
    next();
  }, nextError);
};

/**
 * Gets feed config, dataSource config and adapter config
 */
function makeHandlerGetFeedConfigs(operation) {
  return function(request, response, next) {
    if (operation) request.operation = operation;
    request.config = {};
    composition.chain(handleGetFeedConfig, handleGetDataSourceConfig,
        handleGetAdapterConfig)(request, response, next);
  };
};

function handleResult(successStatus, response) {
  return function(result) {
    if (typeof result == 'object' && result.isError) {
      sendErrorResponse(response, result);
    } else {
      sendOkResponse(response, successStatus, result);
    }
  };
};

function handleError(response) {
  return function(error) {
    sendErrorResponse(response, error);
  };
};

function sendOkResponse(response, status, result) {
  sendResponse(response, status, result ? JSON.stringify(result) : '{}',
      'application/json');
};

function sendErrorResponse(response, error) {
  sendResponse(
      response, error.statusCode || 400,
      error.message || '<no error message>', 'text/plain');
};

function sendResponse(response, status, content, contentType) {
  response.writeHead(status, {
    'Content-Type': contentType,
    'Content-Length': Buffer.byteLength(content, "utf8"),
//    'Access-Control-Allow-Origin': '*'
  });
  response.end(content);
};

function handleReceiveBody(request, response, next) {
  var chunks = [];
  request.addListener('data', function(chunk) {
    chunks.push(chunk);
  });
  request.addListener('end', function() {
    request.body = chunks.join('');
    next(request, response);
  });
};

function invoke(config, operationName, params, requestBody, next) {
  var adapter = require(config.adapter.module);
  var operation = adapter[operationName] || adapter.callOperation;
  if (!operation) {
    next(new Error('adapter "' + config.adapter.module +
        '" should implement operation "' + operationName +
        '" or "callOperation"'));
  } else {
    params.__operationName = operationName;
    if (requestBody) {
      var entry = typeof(requestBody) == 'string' ?
          JSON.parse(requestBody) : requestBody;
      operation(params, entry, config.feed, config.dataSource, next);
    } else {
      operation(params, config.feed, config.dataSource, next);
    }
  }
};

function handleList(request, response) {
  invoke(request.config, 'list', request.params, request.body,
      handleResult(200, response));
};

function handleGet(request, response) {
  invoke(request.config, 'get', request.params, request.body,
      handleResult(200, response));
};

function handleInsert(request, response) {
  invoke(request.config, 'insert', request.params, request.body,
      handleResult(201, response));
};

function handleUpdate(request, response) {
  invoke(request.config, 'update', request.params, request.body,
      handleResult(200, response));
};

function handleRemove(request, response) {
  invoke(request.config, 'remove', request.params, request.body,
      handleResult(200, response));
};

// receives POST request whose body is parameters
function handleReceiveRpc(request, response, next) {
  request.params = JSON.parse(request.body);
  request.operation = request.params.operationName;
  handlePopulateUser(request, response, next);
};

function handleRpc(request, response) {
  request.params.args.userId = request.params.userId;
  invoke(request.config, request.params.operationName, request.params.args,
      request.params.body, handleResult(200, response));
};

function handlePopulateHeaders(request, response, next) {
  if (request.params.args) {
    request.params.args._clientIpAddress = request.headers['x-forwarded-for'] ||
        request.connection.remoteAddress;
    request.params.args._referer = request.headers['referer'];
  }
  next(request, response);
};

function makeHandlerCreateChain(operation, lastHandler, opt_firstHandler) {
  return opt_firstHandler ? composition.chain(
      opt_firstHandler, makeHandlerGetFeedConfigs(operation),
          handlePopulateHeaders,
          handlePopulateUser, loadGroups, handleAuthorize, handleParseQueryParams,
          lastHandler) :
      composition.chain(makeHandlerGetFeedConfigs(operation),
          handlePopulateHeaders,
          handlePopulateUser, loadGroups, handleAuthorize, handleParseQueryParams,
          lastHandler);
};

function handlePopulateUser(request, response, next) {
  if (authn) {
    request.user = authn.getUser(request, response);
  }
  next(request, response);
};

function handlePopulateMe(request, response, next) {
  if (request.params.ownerId == 'me') {
    request.params.ownerId = request.user ? request.user.id : null;
    request.user.viewerIsOwner = true;
  }
  next(request, response);
};

function makeHandlerCreateUserChain(operation, lastHandler, opt_firstHandler) {
  return opt_firstHandler ?
      composition.chain(opt_firstHandler, makeHandlerGetFeedConfigs(operation),
          handlePopulateHeaders,
          handlePopulateUser, handlePopulateMe, handleAuthorize,
          handleParseQueryParams, lastHandler) :
      composition.chain(makeHandlerGetFeedConfigs(operation),
          handlePopulateHeaders,
          handlePopulateUser, handlePopulateMe, handleAuthorize,
          handleParseQueryParams, lastHandler);
};

var authn = null;

function handleStatus(request, response) {
  response.type('.txt').status(200).send('ok');
};

function routes(a, opt_withApiPrefix) {
  authn = a;

  return function(app) {
    var apiPrefix = opt_withApiPrefix ? '/api' : '';

    // -------------
    // Meta endpoint
    app.get(apiPrefix + '/_/status', handleStatus);

    // ------------
    // RPC endpoint
    app.post(apiPrefix + '/rpc', makeHandlerCreateChain(null, handleRpc,
        composition.chain(handleReceiveBody, handleReceiveRpc)));

    // ------------
    // domain feeds
    app.get(apiPrefix + '/:namespace/:feedId',
        makeHandlerCreateChain('list', handleList));
    app.get(apiPrefix + '/:namespace/:feedId/:entryId',
        makeHandlerCreateChain('get', handleGet));
    app.post(apiPrefix + '/:namespace/:feedId',
        makeHandlerCreateChain('insert', handleInsert, handleReceiveBody));
    app.put(apiPrefix + '/:namespace/:feedId/:entryId',
        makeHandlerCreateChain('update', handleUpdate, handleReceiveBody));
    app.delete(apiPrefix + '/:namespace/:feedId/:entryId',
        makeHandlerCreateChain('remove', handleRemove));

    // ----------
    // user feeds
    app.get(apiPrefix + '/:namespace/user/:ownerId/:feedId',
        makeHandlerCreateUserChain('list', handleList));
    app.get(apiPrefix + '/:namespace/user/:ownerId/:feedId/:entryId',
        makeHandlerCreateUserChain('get', handleGet));
    app.post(apiPrefix + '/:namespace/user/:ownerId/:feedId',
        makeHandlerCreateUserChain('insert', handleInsert, handleReceiveBody));
    app.put(apiPrefix + '/:namespace/user/:ownerId/:feedId/:entryId',
        makeHandlerCreateUserChain('update', handleUpdate, handleReceiveBody));
    app.delete(apiPrefix + '/:namespace/user/:ownerId/:feedId/:entryId',
        makeHandlerCreateUserChain('remove', handleRemove));
  };
};

var PRINCIPAL = {
  ANY: '*', OWNER_SELF: 'OWNER_SELF'
};

function isAllowed(request, allowedPrincipals, deniedPrincipals) {
  console.log(request.user, allowedPrincipals, deniedPrincipals);
  console.log(!((request.user && request.user.id) || allowedPrincipals || deniedPrincipals));
  if (request.user && request.user.viewerIsOwner) {
    logger.log('isAllowed() true (viewer is owner)');
    return true;
  }

  if (!((request.user && request.user.id) || allowedPrincipals || deniedPrincipals)) {
    // no authenticated user and no ACLs
    logger.log('isAllowed() true (no authenticated user and no ACLs)');
    return true;
  }

  if (deniedPrincipals) {
    var deniedPrincipal = deniedPrincipals[i];
    for (var i = 0; i < deniedPrincipals.length; i++) {
      if (deniedPrincipal == PRINCIPAL.ANY) {
        logger.log('isAllowed() false (* denied)');
        return false;
      } else if (request.user.id == deniedPrincipal) {
        logger.log('isAllowed() false (' + request.user.id + ' denied)');
        return false;
      } else if (isMember(request, request.user.id, deniedPrincipal)) {
        logger.log('isAllowed() false (' + request.user.id +
            ' member of denied ' + deniedPrincipal + ')');
        return false;
      }
    };
  }

  if (allowedPrincipals) {
    for (var i = 0; i < allowedPrincipals.length; i++) {
      var allowedPrincipal = allowedPrincipals[i];
      if (allowedPrincipal == PRINCIPAL.ANY) {
        logger.log('isAllowed() true (* allowed)');
        return true;
      } else if (allowedPrincipal == PRINCIPAL.OWNER_SELF &&
          request.user.id == request.params.ownerId) {
        logger.log('isAllowed() true (user is owner)');
        return true;
      } else if (request.user.id == allowedPrincipal) {
        logger.log('isAllowed() true (' + request.user.id + ' allowed)');
        return true;
      } else if (isMember(request, request.user.id, allowedPrincipal)) {
        logger.log('isAllowed() true (' + request.user.id +
            ' member of allowed ' + allowedPrincipal + ')');
        return true;
      }
    };
  }

  logger.log('isAllowed() false (fell through)');
  return false;
};

function isMember(request, userId, principalName) {
  if (principalName.indexOf(GROUP_PREFIX) != 0) {
    // principalName is not a group
    return false;
  }

  var groupName = principalName.substring(GROUP_PREFIX.length);
  var group = request.groups[groupName];
  if (group) {
    return group[userId];
  } else {
    // group not found
    return false;
  }
};

function handleAuthorize(request, response, next) {
  if (!request.user) {
    request.user = {};
  }

  // TODO: this should be in its own handler
  request.params.userId = request.user.id;

  var operationConfig = request.config.feed.operations[request.operation];
  if (operationConfig) {
    if (isAllowed(request, operationConfig.allowed, operationConfig.denied)) {
      next(request, response);
    } else {
      sendErrorResponse(response, {statusCode: 403, message: 'Forbidden'});
    }
  } else {
    sendErrorResponse(response, {statusCode: 404, message: 'operation "' +
        request.params.feedId + '.' + request.operation + '" not configured'});
  }
};

function handleParseQueryParams(request, response, next) {
  var parsedUrl = url.parse(request.url);
  var parsedQuery = querystring.parse(parsedUrl.query);
  for (var p in parsedQuery) {
    // should not overwrite existing parameters
    if (typeof(request.params[p]) == 'undefined') {
      request.params[p] = parsedQuery[p];
    }
  }
  next(request, response);
};

function loadGroups(request, response, next) {
  getConfig('groups', withInstance('all'), function(all) {
    request.groups = all;
    next(request, response);
  }, function(error) {
    logger.log('loadGroups warning=', error);
    request.groups = {};
    next(request, response);
  });
};

function call(namespace, feedConfig, operationName, params, requestBody, next,
    nextError) {
  var config = {feed: feedConfig};
  getDataSourceConfig_(namespace, feedConfig.datasource, config, function() {
    getAdapterConfig_(namespace, config.dataSource.adapter, config, function() {
      invoke(config, operationName, params, requestBody, next, nextError);
    }, nextError);
  }, nextError);
};

exports.routes = routes;
exports.handleAuthorize = handleAuthorize;
exports.call = call;
exports.generateClient = require('./browser-client-gen');
