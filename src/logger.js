function toIsoFormat(d) {
  function pad(n) {
    return n < 10 ? '0'+n : n;
  };
  return d.getFullYear() + '-' + pad(d.getMonth() + 1) + '-' + pad(d.getDate()) + 'T' +
      pad(d.getHours()) + ':' + pad(d.getMinutes()) + ':' + pad(d.getSeconds()) + 'Z';
};

function getModuleName(filePath) {
  var lastSlash = filePath.lastIndexOf('/');
  return lastSlash < 0 ? filePath : filePath.substring(lastSlash + 1, filePath.length - 3);
};

var pad = '              ';
function padLeft(s) {
  s = pad + s;
  return s.substring(s.length - pad.length);
};

function create(callerModule) {
  var callerModuleName = getModuleName(callerModule ? callerModule.filename : '???');
  return {
    log: function(message, opt_object) {
      console.log(toIsoFormat(new Date()) + ' [' + padLeft(callerModuleName) + '] ' + message);
      if (opt_object) {
        console.log(opt_object);
      }
    }
  };
};

exports.create = create;
