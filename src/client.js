// client library generated from configuration

var fs = require('fs');
var feedProvider = require('./feed-provider');

var CONF_FEEDS_DIR = (process.env.NODE_FEEDSERVER_CONF_DIR || 'conf') + '/feeds';

function generateClientLibrary(next) {
  var api = {};
  var confFeedsDir = CONF_FEEDS_DIR;
  var fileNames = fs.readdirSync(confFeedsDir);
  fileNames.forEach(function(namespace) {
    var feeds = {};
    api[namespace] = feeds;
    var feedDir = confFeedsDir + '/' + namespace + '/feed';
    var feedNames = fs.readdirSync(feedDir);
    feedNames.forEach(function(feedName) {
      if (feedName.indexOf('.') != 0) {  // skip hidden files
        var feedConfigPath = feedDir + '/' + feedName;
        if (fs.statSync(feedConfigPath).isDirectory()) {
          // TODO: handle user feeds
        } else {
          var feedConfig = fs.readFileSync(feedConfigPath, 'utf8');
          try {
            feedConfig = JSON.parse(feedConfig);
          } catch(e) {
            throw new Error('failed to parse feed config "' + feedConfigPath +
                '"; error="' + e + '" feedConfig=' + feedConfig);
          }
          if (feedConfig.listenChannels) {
            feedConfig.operations.listen = {};  // auto-generated operation
          }

          var feed = {};
          feeds[feedName] = feed;
          for (var operationName in feedConfig.operations) {
            (function(ns, fc, on) {
              feed[operationName] = function(args, body, handleResult, handleError) {
                // this function can be invoked as f(args, entry, handleResult) or f(args, handleResult)
                if (typeof(body) == 'function') {
                  // second variation
                  handleError = handleResult;
                  handleResult = body;
                  body = undefined;
                }
                callApi(ns, fc, on, args, body, handleResult, handleError);
              };
              feed[operationName].ns = ns;
              feed[operationName].fn = feedName;
              feed[operationName].on = on;
            })(namespace, feedConfig, operationName);
          };
        }
      }
    });
  });
  return api;
};

function callApi(namespace, feedConfig, operationName, args, body, handleResult, handleError) {
  feedProvider.call(namespace, feedConfig, operationName, args, body, handleResult, handleError);
};

exports.generate = generateClientLibrary;
