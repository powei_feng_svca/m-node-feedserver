/**
 * A simple Google Drive adapter.
 * @author luke.chang@svca.cc (Luke Chang)
 */
var https = require('https'),
    $ = require('jquery');

var apiKey = 'AIzaSyDDAIBPziJ_h6S68_maqoMWSdnKTqg4004';

function get(params, feedConfig, dataSourceConfig, next) {
  var file = feedConfig.operations.get[params.file],
      options = {
        hostname: 'www.googleapis.com',
        path: '/drive/v2/files/' + file.id + '?key=' + apiKey
      };
  https.get(options, function (res) {
    var chunks = [];
    res.on('data', function (chunk) {
      chunks.push(chunk);
    });

    res.on('end', function () {
      var body = chunks.join('');
      if (res.statusCode < 300) {
        var feed = JSON.parse(body);
        var doc = {title: feed.title};

        https.get(feed.exportLinks['text/html'], function (res) {
          var chunks = [];
          res.on('data', function (chunk) {
            chunks.push(chunk);
          });
          res.on('end', function () {
            var $html = $(chunks.join(''));
            doc.body = $('body', $html).html();
            next(doc);
          });
        });
      } else {
        next(new Error(body));
      }
    });
  });
}

exports.get = get;

