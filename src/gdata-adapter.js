/*
 * GData adapter
 */

var fs = require('fs');
var httpClient = require('http-client');
var logger = require('logger').create(module);
var oauth2legged = require('oauth-2legged');
var querystring = require('querystring');
var template = require('./template');

function extend(object, prototype) {
  object.__proto__ = prototype;
  return object;
};

function getFileContent(filePath, next, handleError) {
  logger.log(filePath);
  fs.readFile(filePath, 'utf8', function(error, content) {
    error ? handleError(error) : next(content.replace('\n', ''));
  });
};

function sendGDataRequest(method, url, headers, body, domain, consumerSecretFilePath, viewerId, next) {
  getFileContent(consumerSecretFilePath, function(consumerSecret) {
    var request = {method: method, url: url + '?alt=json', userId: viewerId};
    var oauthRequest = oauth2legged.decorateRequest(request, domain, consumerSecret);
    httpClient.send(method, oauthRequest.url, headers, body, function(response) {
      if (response.statusCode < 300) {
        var resource = JSON.parse(response.data);
        next(resource.feed || resource.entry);
      } else {
        if (response.statusCode == 401) {
          sendGDataRequest(method, url, headers, body, domain, consumerSecretFilePath, viewerId, next);
          logger.log('retried');
        } else {
          var error = new Error(response.data);
          error.isError = true;
          error.statusCode = response.statusCode;
          next(error);
        }
      }
    });
  });
};

// TODO: Support max-results and start-index
// TODO: Support pagination
function list(params, feedConfig, dataSourceConfig, next) {
  var viewerId = params.ownerId + '@' + dataSourceConfig.domain;  // TODO: fix
  sendGDataRequest('GET', feedConfig.url, {}, null, dataSourceConfig.domain,
      dataSourceConfig.consumerSecretFilePath, viewerId, next);
};

function get(params, feedConfig, dataSourceConfig, next) {
  var viewerId = params.ownerId + '@' + dataSourceConfig.domain;  // TODO: fix
  sendGDataRequest('GET', feedConfig.url + '/' + querystring.escape(params.entryId), {}, null,
      dataSourceConfig.domain, dataSourceConfig.consumerSecretFilePath, viewerId, next);
};

function insert(params, entry, feedConfig, dataSourceConfig, next) {
  var viewerId = params.ownerId + '@' + dataSourceConfig.domain;  // TODO: fix
  sendGDataRequest('POST', feedConfig.url, {'Content-Type': 'application/atom+xml'},
      template.fill(feedConfig.newEntryAtom, entry), dataSourceConfig.domain,
      dataSourceConfig.consumerSecretFilePath, viewerId, next);
};

// TODO: remove assumption of primary key being "id"
function update(params, entry, feedConfig, dataSourceConfig, next) {
};

// TODO: remove assumption of primary key being "id"
function remove(params, feedConfig, dataSourceConfig, next) {
};

function createError(message) {
  var error = new Error(message);
  error.isError = true;
  error.stack = null;
  return error;
};

var ERROR = {
  NOT_FOUND: createError('not found')
};

exports.list = list;
exports.get = get;
exports.insert = insert;
exports.update = update;
exports.remove = remove;
exports.ERROR = ERROR;
