var fs = require('fs');

// -----------------
// simple templating

function getEnv(name) {
  return name.indexOf('env.') == 0 ? process.env[name.substring(4)] : null;
};

/**
 * Fills a template with values.
 * @param template Template in format ...${variableName(:annotation}}...
 * @param values Map to look up value by name
 * @param opt_valueDecorator function(name, annotation) => decoratedValue
 * @param opt_skipNull {boolean} Whether to skip variables whose value is null
 *     or undefined
 * @return Template with all variables filled
 */
function fill(template, values, opt_valueDecorator, opt_skipNull) {
  template = fillParams(template, values, opt_valueDecorator, opt_skipNull);
  return fillEnvVars(template, opt_valueDecorator, opt_skipNull);
};

function fillParams(template, values, opt_valueDecorator, opt_skipNull) {
  return fillValues(template, /\$\{(\w+)(?::(\w+))?\}/g, '\$\{', values,
      opt_valueDecorator, opt_skipNull);
};

function fillEnvVars(template, opt_valueDecorator, opt_skipNull) {
  return fillValues(template, /\$env\{(\w+)(?::(\w+))?\}/g, '\$env\{',
      process.env, opt_valueDecorator, opt_skipNull);
};

function fillValues(
    template, pattern, varPrefix, values, opt_valueDecorator, opt_skipNull) {
  var s = template;
  for (var match = pattern.exec(template); match != null;
      match = pattern.exec(template)) {
    var name = match[1];
    var value = getEnv(name) || values[name]
    if (value || !opt_skipNull) {
      var annotation = match[2];
      var annotatedName =
          varPrefix + match[1] + (match[2] ? ':' + match[2] : '') + '\}';
      value = opt_valueDecorator ?
          opt_valueDecorator(annotation, value) : value;
      s = s.replace(annotatedName, typeof(value) == 'object' ?
          JSON.stringify(value) : value);
    }
  }
  return s;
};

function fillFile(templateFilePath, values, opt_valueDecorator, next) {
  fs.readFile(templateFilePath, 'utf8', function(error, content) {
    if (error) { 
      error.isError = true;
      next(error)
    } else {
      next(fill(content, values, opt_valueDecorator));
    }
  });
};

exports.fill = fill;
exports.fillFile = fillFile;
