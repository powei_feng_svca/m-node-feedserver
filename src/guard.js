/**
 * Guards a handler with authentication.
 */

var authn = IS_DEV ? require('auth-fake') : require('auth-oauth2');
var logger = require('logger').create(module);
var url = require('url');

function makeHandlerGuard(loginPath, excludedPaths, guardedHandler) {
  return function(request, response, next) {
    function applicable(parsedUrl, excludedPaths) {
      for (var i = 0; i < excludedPaths.length; i++) {
        if (parsedUrl.pathname.indexOf(excludedPaths[i]) == 0) {
          // path starts with the excluding pattern
          return false;
        }
      }
      return true;
    };

    var parsedUrl = url.parse(request.url);
    if (applicable(parsedUrl, excludedPaths)) {
      var user = authn.getUser(request, response);
      if (user) {
        request.user = user;
        guardedHandler(request, response, next);
      } else {
        var parsedUrl = url.parse(request.url);
        var location = loginPath + '?' + parsedUrl.pathname;
        logger.log('No cookie; redirecting to ' + location);
        response.writeHead(302, {
            Location: location,
           'Set-Cookie': 'continuation=' + parsedUrl.pathname + '; Path=/'
        });
        response.end('');
      }
    } else {
      next();
    }
  };
};

exports.makeHandlerGuard = makeHandlerGuard;
