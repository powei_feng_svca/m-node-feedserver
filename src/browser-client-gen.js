// browser API client library generator

var api = require('./client').generate();
var fs = require('fs');
var path = require('path');

var BROWSER_SCRIPTS_DIR = path.join(__dirname, '../www/js');
//var clientScripts = fs.readdirSync(BROWSER_SCRIPTS_DIR).map(function(fileName) {
//  return fs.readFileSync(path.join(BROWSER_SCRIPTS_DIR, fileName), 'utf-8');
//});
var clientScripts = [];

function generate(namespace) {
  var code = clientScripts.concat('var ownerId;\nvar api = {' + namespace + ': {\n');

  generateNamespace(code, namespace, false);
  generateNamespace(code, namespace, true);
  return code.join('');
};

function generateNamespace(code, namespace, userFeeds) {
  var feeds = userFeeds? api[namespace].user : api[namespace];

  if (userFeeds) {
    code.push('api.' + namespace + '.user = function(opt_ownerId) {\n');
    code.push('  var ownerId = opt_ownerId || "me";\n');
    code.push('  return {\n');
  }

  for (var feedName in feeds) {
    if (feedName == 'user') continue;

    code.push('  ' + feedName + ':\n');
    var feed = feeds[feedName];
    code.push('  {\n');
    for (var operationName in feed) {
      var operation = feed[operationName];
      code.push('    ' + operationName + ': function(args, body, handleResult, handleError) {\n');
      code.push('      var ns = "' + operation.ns + '";\n');
      code.push('      var fc = "' + operation.fn + '";\n');
      code.push('      var on = "' + operation.on + '";\n');
      code.push('      (' + operation.toString() + '\n');
      code.push('      )(args, body, handleResult, handleError);\n');
      code.push('    },\n');
    }
    code.push('  },\n');
  };

  if (userFeeds) {
    code.push('  };\n');
    code.push('};\n');
  } else {
    code.push('}, $: {handleError: function(e){console.log(e)}}};\n');
  }
};

if (require.main == module) {
  if (process.argv.length != 3) {
    console.log('usage:', process.argv[0], process.argv[1], '<namespace>');
  } else {
    console.log(generate(process.argv[2]));
  }
} else {
  module.exports = generate;
}
