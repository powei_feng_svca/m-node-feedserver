var crypto = require('crypto');
var fs = require('fs');
var logger = require('logger').create(module);
var querystring = require('querystring');
var url = require('url');

var OAUTH_SIGNATURE = 'oauth_signature';

function percentEncode(s) {
  return s ? querystring.escape(s).replace('+', '%20').replace('*', '%2A').replace('%7E', '~') : '';
};

function normalizeUrl(parsedUrl) {
  var protocol = parsedUrl.protocol;
  var authority = (protocol == 'http:' && parsedUrl.port == 80 ||
      protocol == 'https:' && parsedUrl.port == 443) ?
      (parsedUrl.auth ? parsedUrl.auth + '@' : '') + parsedUrl.hostname :
      parsedUrl.host;
  var path = parsedUrl.pathname || '/';
  return protocol + '//' + authority + path;
};

function normalizeParameters(requestParams) {
  var paramNames = [];
  for (var name in requestParams) {
    if (name != OAUTH_SIGNATURE) {
      paramNames.push(name);
    }
  }
  var params = [];
  paramNames.sort().forEach(function(name) {
    var param = {};
    param[name] = requestParams[name];
    params.push(querystring.stringify(param));
  });
  return params.join('&');
};

function getBaseString(request, parsedUrl, requestParams) {
  return percentEncode(request.method.toUpperCase()) + '&' +
      percentEncode(normalizeUrl(parsedUrl)) + '&' +
      percentEncode(normalizeParameters(requestParams));
};

exports.verify = function(externalUrlHost, certFilePath) {
  var cert = fs.readFileSync(certFilePath);
  logger.log('certFilePath=' + certFilePath);
  logger.log('cert=' + cert);

  function isSignatureValid(message, signature) {
    var verify = new crypto.Verify();
    verify.init('RSA-SHA1');
    verify.update(message);
    return verify.verify(cert, signature, 'base64');
  };

  return function(request, response, next) {
    var fullUrl = externalUrlHost + request.url;
    var parsedUrl = url.parse(fullUrl);
    var requestParams = querystring.parse(parsedUrl.query);
    var baseString = getBaseString(request, parsedUrl, requestParams);
    var signature = requestParams[OAUTH_SIGNATURE];

    if (signature && isSignatureValid(baseString, signature)) {
      // copy parameters
      request.params = request.params || {};
      for (var name in requestParams) {
        var value = requestParams[name];
        request.params[name] = value;
      }

      // set authenticated viewer email
      request.user = {id: request.params.opensocial_viewer_email};
      logger.log('request.user.id=' + request.user.id + ' (signedFetch)');
      next();
    } else {
      response.writeHead(403, {'Content-Type': 'text/plain'});
      response.end('signed fetch failed to verify; forbidden');
    }
  };
};
