// --------------------
// function composition

/**
 * Executes a chain of functions one by one asynchronously.
 * @param arguments {Array} Array of function(...)
 * @return {Function} Function that executes the array of functions one by one
 */
function chain() {
  var functions = arguments;
  var chainLength = functions.length;
  if (chainLength == 0) {
    return function(request, response, next){
      next(request, response);
    };
  } else {
    var chained = [];
    chained[chainLength - 1] = functions[chainLength - 1];
    for (var i = chainLength - 2; i >= 0; i--) {
      chained[i] = (function(i) {
        return function(request, response, next) {
          functions[i](request, response, function(request, response) {
            chained[i + 1](request, response, next);
          });
        };
      })(i);
    }
    return chained[0];
  }
};

exports.chain = chain;
