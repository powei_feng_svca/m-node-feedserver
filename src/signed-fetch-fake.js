var logger = require('logger').create(module);

var fakeUserId = null;

function setFakeUserId(fuser) {
  fakeUserId = fuser;
};

function provideTestUser() {
  return function(request, response, next) {
    request.params = request.params || {};
    request.user = {id: fakeUserId};
    logger.log('request.user=' + request.user + ' (fake)');
    next();
  };
};

exports.setFakeUserId = setFakeUserId;
exports.verify = provideTestUser;
