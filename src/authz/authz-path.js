// path based access control

var fs = require('fs');
var logger = require('../logger').create(module);
var url = require('url');

var ENV = global.INSTANCE || process.env.NODE_FEEDSERVER_ENV;

var groups = null;

/**
 * Adds runtime environment (e.g. DEV, TST, STG and PRD) to a name.  For
 * example, if ENV is null, then withInstance('cedartc-db') is 'cedartc-db.
 * If ENV is 'DEV', then withInstance('cedartc-db') is 'cedartc-db.DEV'.
 */
function withInstance(name) {
  return ENV ? name + '.' + ENV : name;
};

/**
 * @param pathPrefixes {object} Prefixes to principals map.  E.g.:
 *   { "/admin", ["user1@domain.com", "g:group1@domain.com"] }
 */
function makeHandlerAuthorize(pathPrefixes, opt_groups) {
  groups = opt_groups || null;

  return function(request, response, next) {
    var parsedUrl = url.parse(request.url);
    var principals = getPrincipals(parsedUrl, pathPrefixes);
    if (principals) {
      if (!groups) {
        fs.readFile(withInstance('conf/groups/all'), 'utf8', function(error, content) {
          if (error) {
            response.writeHead(500, {'Content-Type': 'text/plain'});
            response.end(error.toString(), 'utf8');
          } else {
            groups = JSON.parse(content);
            checkAccess(principals, groups, request, response, next);
          }
        });
      } else {
        checkAccess(principals, groups, request, response, next);
      }
    } else {
      // no access control
      next();
    }
  };
};

/**
 * Gets the principals defined for the path that is a prefix of the path of
 * the current request.
 */
function getPrincipals(parsedUrl, pathPrefixes) {
  for (var pathPrefix in pathPrefixes) {
    if (parsedUrl.pathname.indexOf(pathPrefix) == 0) {
      return pathPrefixes[pathPrefix];
    }
  }
  return null;
};

function checkAccess(principals, groups, request, response, next) {
  if (isAllowed(request.user.id, principals, groups)) {
    logger.log('user allowed: ' + request.user.id);
    next();
  } else {
    logger.log('user denied: ' + request.user.id);
    response.writeHead(403, {'Content-Type': 'text/plain'});
    response.end(request.user.id + ' access denied', 'utf8');
  }
};

var GROUP_PREFIX = 'g:';

function isAllowed(userId, principals, groups) {
  for (var i = 0; i < principals.length; i++) {
    var principal = principals[i];
    if (userId == principal) {
      return true;
    } else if (isGroup(principal) && isMember(userId, principal)) {
      return true;
    }
  }

  return false;
};

function isGroup(name) {
  return name.indexOf(GROUP_PREFIX) == 0;
};

function isMember(userId, groupName) {
  if (groupName.indexOf(GROUP_PREFIX) == 0) {
    groupName = groupName.substring(GROUP_PREFIX.length);
  }
  var group = groups[groupName];
  return group && group[userId];
};

exports.makeHandlerAuthorize = makeHandlerAuthorize;
exports.isAllowed = isAllowed;
exports.getPrincipals = getPrincipals;
