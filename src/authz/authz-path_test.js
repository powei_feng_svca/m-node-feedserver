var assert = require("assert")
var authz = require('./authz-path');

var principals = ['user1@x.com', 'user2@x.com', 'g:group1'];
var protectedPaths = {'/admin/': principals};
var groups = {
  group1: {
    "user3@x.com": true,
    "user4@x.com": true
  }
};

authz.makeHandlerAuthorize(protectedPaths, groups);

describe('authz-path', function() {
  describe('#getPrincipals()', function() {
    it('should return some', function() {
      assert.equal(true, !!authz.getPrincipals({pathname: '/admin/abc'}, protectedPaths));
    })
  })
});

describe('authz-path', function() {
  describe('#getPrincipals()', function() {
    it('should return none', function() {
      assert.equal(false, !!authz.getPrincipals({pathname: '/open'}, protectedPaths));
    })
  })
});

describe('authz-path', function() {
  describe('#isAllowed()', function() {
    it('should be allowed', function() {
      assert.equal(true, authz.isAllowed('user1@x.com', principals, groups));
      assert.equal(true, authz.isAllowed('user2@x.com', principals, groups));
      assert.equal(true, authz.isAllowed('user3@x.com', principals, groups));
      assert.equal(true, authz.isAllowed('user4@x.com', principals, groups));
    })
  })
});

describe('authz-path', function() {
  describe('#isAllowed()', function() {
    it('should NOT be allowed', function() {
      assert.equal(false, authz.isAllowed('userA@x.com', principals, groups));
    })
  })
});
