/*
 * GData docs API adapter (via GData Java client lib)
 */

var child = require('child_process');
var fs = require('fs');
var logger = require('logger').create(module);
var template = require('./template');

function extend(object, prototype) {
  object.__proto__ = prototype;
  return object;
};

function readFileContent(filePath, next) {
  fs.readFile(filePath, 'utf8', function(error, content) {
    error ? next(error) : next(content.replace('\n', ''));
  });
};

function writeFileContent(filePath, fileContent, next) {
  fs.writeFile(filePath, fileContent, function(error) {
    if (error) throw error;
    next();
  });
};

function getTempDocFilePath(docId) {
  return '/tmp/doc-' + docId + '-' + (Math.floor(Math.random() * 1000) + 1000) + '.html';
};

function listDocuments(domainName, consumerSecretFilePath, userId, next) {
  readFileContent(consumerSecretFilePath, function(consumerSecret) {
    var command = ['cd src/gdata-docs-adapter.java && ./docs-tool.sh'];
    command.push('listDocuments');
    command.push(domainName);
    command.push('"' + consumerSecret + '"');
    command.push(userId);
    logger.log('exec: ' + command.join(' '));
    child.exec(command.join(' '), function (error, stdout, stderr) {
      if (error != null) {
        var error = new Error(stderr);
        error.isError = true;
        logger.log('exec error');
        next(error);
      } else {
        logger.log('exec ok');
        next(JSON.parse(stdout));
      }
    });
  });
};

function createDocument(domainName, consumerSecretFilePath, userId, title, documentTemplateFilePath,
    taUserId, teacherGroupId, next) {
  readFileContent(consumerSecretFilePath, function(consumerSecret) {
    var command = ['cd src/gdata-docs-adapter.java && ./docs-tool.sh'];
    command.push('createHomeworkDocument');
    command.push(domainName);
    command.push('"' + consumerSecret + '"');
    command.push(userId);
    command.push('"' + title + '"');
    command.push('"' + documentTemplateFilePath + '"');
    command.push(taUserId);
    command.push(teacherGroupId);
    logger.log('exec: ' + command.join(' '));
    child.exec(command.join(' '), function (error, stdout, stderr) {
      if (error != null) {
        var error = new Error(stderr);
        error.isError = true;
        logger.log('exec error');
        next(error);
      } else {
        logger.log('exec ok');
        next(JSON.parse(stdout));
      }
    });
  });
};

function getDocument(domainName, consumerSecretFilePath, userId, docId, next) {
  readFileContent(consumerSecretFilePath, function(consumerSecret) {
    var command = ['cd src/gdata-docs-adapter.java && ./docs-tool.sh'];
    command.push('getHomeworkDocument');
    command.push(domainName);
    command.push('"' + consumerSecret + '"');
    command.push(userId);
    command.push(docId);
    logger.log('exec: ' + command.join(' '));
    child.exec(command.join(' '), function (error, stdout, stderr) {
      if (error != null) {
        var error = new Error(stderr);
        error.isError = true;
        logger.log('exec error');
        next(error);
      } else {
        logger.log('exec ok');
        next(JSON.parse(stdout));
      }
    });
  });
};

function updateDocument(domainName, consumerSecretFilePath, userId, docId, content, next) {
  readFileContent(consumerSecretFilePath, function(consumerSecret) {
    var docFilePath = getTempDocFilePath(docId);
    writeFileContent(docFilePath, content, function() {
      var command = ['cd src/gdata-docs-adapter.java && ./docs-tool.sh'];
      command.push('updateHomeworkDocument');
      command.push(domainName);
      command.push('"' + consumerSecret + '"');
      command.push(userId);
      command.push(docId);
      command.push(docFilePath);
      logger.log('exec: ' + command.join(' '));
      child.exec(command.join(' '), function (error, stdout, stderr) {
        if (error != null) {
          var error = new Error(stderr);
          error.isError = true;
          logger.log('exec error');
          next(error);
        } else {
          logger.log('exec ok');
          next(JSON.parse(stdout));
        }
      });
    });
  });
};

function list(params, feedConfig, dataSourceConfig, next) {
  listDocuments(dataSourceConfig.domain, dataSourceConfig.consumerSecretFilePath, params.ownerId, next);
};

function get(params, feedConfig, dataSourceConfig, next) {
  getDocument(dataSourceConfig.domain, dataSourceConfig.consumerSecretFilePath, params.ownerId,
      params.entryId, next);
};

function insert(params, entry, feedConfig, dataSourceConfig, next) {
  createDocument(dataSourceConfig.domain, dataSourceConfig.consumerSecretFilePath, params.ownerId,
      entry.title, feedConfig.newDocumentTemplateFilePath, entry.taUserId, feedConfig.teacherGroupId, next);
};

function update(params, entry, feedConfig, dataSourceConfig, next) {
  updateDocument(dataSourceConfig.domain, dataSourceConfig.consumerSecretFilePath, params.ownerId,
      params.entryId, entry.html, next);
};

function remove(params, feedConfig, dataSourceConfig, next) {
  next();
};

function createError(message) {
  var error = new Error(message);
  error.isError = true;
  error.stack = null;
  return error;
};

var ERROR = {
  NOT_FOUND: createError('not found')
};

exports.list = list;
exports.get = get;
exports.insert = insert;
exports.update = update;
exports.remove = remove;
exports.ERROR = ERROR;
