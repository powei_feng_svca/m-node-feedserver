// goal
var http = require('http');
var cornerstone = require('cornersotne');
var lily = require('google-api-provider');
var oauth = require('google-oauth');

var log = require('morgan');
var verifySignedFetch = oauth.verifySignedFetch;
var translateLily = lily.translateToFeedServer;
var fork = cornerstone.fork;

var receiveBody = feedserver.receiveBody;
var getConfig = feedserver.getFeedConfigs;
var authorize = feedserver.handleAuthorize;

var list = feedserver.list;
var get = feedserver.get;
var insert = feedserver.insert;
var update = feedserver.update;
var remove = feedserver.remove;

var routes = {
    // domain feeds
    '/:namespace/:feedId': [getConfig('list'), authorize, list],
    '/:namespace/:feedId/:entryId': [getConfig('get'), authorize, get],
    '/:namespace/:feedId': [receiveBody, getConfig('insert'), authorize, insert],
    '/:namespace/:feedId/:entryId': [receiveBody, getConfig('update'), authorize, update],
    '/:namespace/:feedId/:entryId': [getConfig('remove'), authorize, remove],

    // user feeds
    '/:namespace/user/:ownerId/:feedId': [getConfig('list'), authorize, list],
    '/:namespace/user/:ownerId/:feedId/:entryId': [getConfig('get'), authorize, get],
    '/:namespace/user/:ownerId/:feedId': [receiveBody, getConfig('insert'), authorize, insert],
    '/:namespace/user/:ownerId/:feedId/:entryId': [receiveBody, getConfig('update'), authorize, update],
    '/:namespace/user/:ownerId/:feedId/:entryId': [getConfig('remove'), authorize, remove],

    // lily
    '/:namespace/rpc': [translateLily, fork(routes)]
};

http.createServer(
    log,
    verifySignedFetch('http://cornerstone.dyndns.info:8081', './igoogle.pem'),
    fork(routes)
).listen(8081);
console.log('Serving on port *:8081');
