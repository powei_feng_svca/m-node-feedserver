var assert = require('assert');
var composition = require('./composition');

function plus10(request, response, next) {
  request.input += 10;
  console.log('plus10()');
  next(request, response);
};

function minus6(request, response, next) {
  request.input -= 6;
  console.log('minus6()');
  next(request, response);
};

function output(request, response, next) {
  response.output = request.input;
  console.log('output()');
  next(request, response);
};

function verify4(request, response, next) {
  assert.equal(response.output, 4);
  console.log('verify4()');
  next(request, response);
};

function verify10(request, response, next) {
  assert.equal(response.output, 10);
  console.log('verify10()');
  next(request, response);
};

var endCalled;

function start(request, response, next) {
  endCalled = false;
  console.log('start()');
  next(request, response);
};

function end(request, response) {
  endCalled = true;
  console.log('end()');
};

function test0() {
  composition.chain(start, plus10, minus6, output, verify4)({input: 0}, {output:null}, end);
  assert.equal(endCalled, true);
};

function test1() {
  composition.chain(start, plus10, output, verify10)({input: 0}, {output: null}, end);
  assert.equal(endCalled, true);
};

function test2() {
  var chain0 = composition.chain(start, plus10);
  var chain1 = composition.chain(minus6, output, verify4);
  composition.chain(chain0, chain1)({input: 0}, {output:null}, end);
  assert.equal(endCalled, true);
};

test0();
test1();
test2();
