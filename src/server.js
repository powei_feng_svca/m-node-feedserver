PORT = process.env.NODE_FEEDSERVER_PORT || 8002;

var authn = require('./authn/auth-fake');
var express = require('express');
var feedServerProvider = require('./feed-provider');
var morgan = require('morgan');
var generateClient = require('./browser-client-gen');

function serveClient(namespace) {
  return function(request, response) {
    response.writeHead(200, {'Content-Type': 'application/javascript'});
    response.end(generateClient(namespace));
  };
}

function allowCors(request, response, next) {
//  response.header('Access-Control-Allow-Origin', '*');
  response.header('Access-Control-Allow-Headers', 'Content-Type');
  response.header('Content-Type', 'application/json');
  next();
}

//LOGIN_PATH = '/s/login-oauth2.html';
//var app = express();
//var guard = require('./guard');
//var authz = require('authz-path');
//STATIC_AUTH = process.env.NODE_FEEDSERVER_STATIC_AUTH || __dirname + '/../www';
//app.use(express.static(STATIC_AUTH, {maxAge: 300000}));
//STATIC_ROOT = process.env.NODE_FEEDSERVER_STATIC_ROOT || process.env.HOME + '/www';
//var protectedPaths = {
//  '/admin/': ['g:administrators']
//};
//app.use(authn.makeHandlerAuthenticate(LOGIN_PATH, ['/s', '/auth', '/favicon.ico', '/api/_/status']));
//app.use(authz.makeHandlerAuthorize(protectedPaths));
//app.use(guard.makeHandlerGuard(LOGIN_PATH, ['/s', '/auth', '/favicon.ico', '/api/_/status'],
//    express.static(STATIC_ROOT, {maxAge: 300000})));
// hack for jqGrid's lack of support for client side form editing
/*
app.post('/admin/clientArray', function(request, response) {
  var body = 'jqGrid hack supported';
  response.setHeader('Content-Type', 'text/plain');
  response.setHeader('Content-Length', body.length);
  response.end(body);
});
 */
function initApp(app) {
  authn.routes(app);
  feedServerProvider.routes(authn, true)(app);
  app.use(morgan(':method :url :status :response-time'));
  app.use(allowCors);
  app.get('/js/api-client.js', serveClient('svca'));
}

if (require.main == module) {
  var app = express();
  initApp(app);
  app.listen(PORT);
  console.log('Serving listening on port *:' + PORT);
} else {
  module.exports = initApp;
}
