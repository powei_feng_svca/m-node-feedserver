package org.cedartc.document;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import com.google.gdata.client.docs.DocsService;
import com.google.gdata.data.MediaContent;
import com.google.gdata.data.media.MediaSource;
import com.google.gson.Gson;

public class GetHomeworkDocument extends AbstractCommand {

  @Override
  public String execute(String[] args) throws Exception {
    String domainName = args[0];
    String cs = args[1];
    String userId = args[2];
    String docId = args[3];
    logArguments(args);

    if (docId == null || "null".equals(docId)) {
      throw new NullPointerException("docId null");
    }

    // download file
    DocsService client = createClient(domainName, cs);
    String exportUrl = "https://docs.google.com/feeds/download/documents/Export?docId=" +
        docId + "&exportFormat=html&xoauth_requestor_id=" + userId;
    String exportFilePath = getDownloadFilePath(docId);
    downloadFile(client, exportUrl, exportFilePath);

    // read and delete file
    File downloadedFile = new File(exportFilePath);
    String html = readFileContent(downloadedFile);
    downloadedFile.delete();

    // return file content
    Map<String, String> map = new HashMap<String, String>();
    map.put("html", html);
    return new Gson().toJson(map);
  }

  @Override
  public String[] getArgumentNames() {
    return new String[]{"domainName", "cs", "userId", "docId"};
  }

  protected String getDownloadFilePath(String docId) {
    return "/tmp/doc-" + docId + "-" + getRandomNumber(1000) + ".html";
  }
  
  protected void downloadFile(DocsService client, String exportUrl, String filePath)
      throws Exception {
    MediaContent mc = new MediaContent();
    mc.setUri(exportUrl);
    MediaSource ms = client.getMedia(mc);

    InputStream is = null;
    FileOutputStream os = null;
    try {
      is = ms.getInputStream();
      os = new FileOutputStream(filePath);

      int c;
      while ((c = is.read()) != -1) {
        os.write(c);
      }
    } finally {
      if (is != null) {
        is.close();
      }
      if (os != null) {
        os.flush();
        os.close();
      }
    }
  }
}
