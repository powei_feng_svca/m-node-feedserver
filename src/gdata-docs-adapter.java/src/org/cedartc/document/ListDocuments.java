package org.cedartc.document;

import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.common.base.Function;
import com.google.common.collect.Lists;
import com.google.gdata.client.docs.DocsService;
import com.google.gdata.data.Person;
import com.google.gdata.data.docs.DocumentListEntry;
import com.google.gdata.data.docs.DocumentListFeed;
import com.google.gdata.util.common.base.Join;
import com.google.gson.Gson;

public class ListDocuments extends AbstractCommand {

  @Override
  public String execute(String[] args) throws Exception {
    String domainName = args[0];
    String cs = args[1];
    String userId = args[2];
    logArguments(args);

    DocsService client = createClient(domainName, cs);
    URL feedUrl = new URL(DOCS_URL + "?xoauth_requestor_id=" + userId + "&max-results=200");
    DocumentListFeed documents = client.getFeed(feedUrl, DocumentListFeed.class);
    Collections.sort(documents.getEntries(), new Comparator<DocumentListEntry>() {
      @Override
      public int compare(DocumentListEntry e1, DocumentListEntry e2) {
        long lastMod1 = e1.getUpdated().getValue();
        long lastMod2 = e2.getUpdated().getValue();
        return lastMod1 < lastMod2 ? 1 : lastMod1 > lastMod2 ? -1 : 0;
      }
    });
    List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
    for (DocumentListEntry entry : documents.getEntries()) {
      Map<String, Object> doc = new HashMap<String, Object>();
      doc.put("title", entry.getTitle().getPlainText());
      doc.put("authors", Join.join(",", Lists.transform(entry.getAuthors(), new Function<Person, String>(){
        @Override
        public String apply(Person person) {
          return person.getEmail();
        }
      })));
      doc.put("updatedAt", entry.getUpdated().getValue());
      doc.put("href", entry.getHtmlLink().getHref());
      list.add(doc);
    }
    System.err.println("count=" + list.size());
    return new Gson().toJson(list);
  }

  @Override
  public String[] getArgumentNames() {
    return new String[]{"domainName", "cs", "userId"};
  }
}
