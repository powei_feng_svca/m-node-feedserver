package org.cedartc.document;

import java.util.HashMap;
import java.util.Map;

public class Tool {

  protected static Map<String, Command> commands = new HashMap<String, Command>();
  static {
    addCommand(new CreateHomeworkDocument());
    addCommand(new GetHomeworkDocument());
    addCommand(new UpdateHomeworkDocument());
    addCommand(new ListDocuments());
  }

  public static void main(String[] args) {
    if (args.length < 1) {
      System.err.println("usage: <command> <arg> <arg> ...");
    } else {
      Command command = findCommand(args[0]);
      if (command == null) {
        System.err.println("commmand '" + args[0] + "' not found");
        System.err.println(commands);
      } else {
//        if (args.length - 1 != command.getArgumentNames().length) {
//          command.showHelp();
//        } else {
          String[] argsOnly = new String[args.length - 1];
          for (int i = 1; i < args.length; i++) {
            argsOnly[i - 1] = args[i];
          }
          try {
            System.out.println(command.execute(argsOnly));
          } catch (Exception e) {
            // e.printStackTrace(System.err);
            System.err.println("Error: " + e.getMessage());
            System.exit(1);
          }
//        }
      }
    }
  }

  protected static Command findCommand(String name) {
    return commands.get(name.toLowerCase());
  }

  protected static void addCommand(Command command) {
    commands.put(command.getName(), command);
  }
}
