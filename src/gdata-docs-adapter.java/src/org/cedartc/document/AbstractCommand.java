package org.cedartc.document;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Map;

import com.google.gdata.client.authn.oauth.GoogleOAuthParameters;
import com.google.gdata.client.authn.oauth.OAuthException;
import com.google.gdata.client.authn.oauth.OAuthHmacSha1Signer;
import com.google.gdata.client.docs.DocsService;

public abstract class AbstractCommand implements Command {

  public static final String DOCS_URL = "https://docs.google.com/feeds/default/private/full";

  protected String convertMapToJson(Map<String, Object> map) {
    StringBuilder sb = new StringBuilder();
    sb.append('{');
    boolean first = true;
    for (Map.Entry<String, Object> entry : map.entrySet()) {
      if (!first) {
        sb.append(',');
      }
      sb.append('"');
      sb.append(entry.getKey());
      sb.append('"');
      sb.append(':');
      sb.append('"');
      sb.append(entry.getValue());
      sb.append('"');
      first = false;
    }
    sb.append('}');
    return sb.toString();
  }

  protected DocsService createClient(String domainName, String cs)
      throws OAuthException {
    DocsService client = new DocsService("test");
    GoogleOAuthParameters oauthParameters = new GoogleOAuthParameters();
    oauthParameters.setOAuthConsumerKey(domainName);
    oauthParameters.setOAuthConsumerSecret(cs);
    client.setOAuthCredentials(oauthParameters, new OAuthHmacSha1Signer());
    return client; 
  }

  protected void logArguments(String[] args) {
    String[] argNames = getArgumentNames();
    if (args.length < argNames.length) {
      throw new IllegalArgumentException();
    }
    for (int i = 0; i < argNames.length; i++) {
      String name = argNames[i];
      if (!"cs".equals(name)) {
        String value = args[i];
        System.err.println(name + "='" + value + "'");
      }
    }
  }

  public void showHelp() {
    System.err.print("usage: ");
    System.err.print(getName());
    for (String name: getArgumentNames()) {
      System.err.print(' ');
      System.err.print(name);
    }
    System.err.println();
  }

  public String getName() {
    return getClass().getSimpleName().toLowerCase();
  }

  protected int getRandomNumber(int range) {
    return (int) (Math.random() * range) + range;
  }

  protected String readFileContent(String filePath) throws IOException {
    return readFileContent(new File(filePath));
  }

  protected String readFileContent(File file) throws IOException {
    BufferedInputStream bis = new BufferedInputStream(new FileInputStream(file));
    byte[] fileContents = new byte[(int) file.length()];
    bis.read(fileContents);
    return new String(fileContents);
  }
}
