package org.cedartc.document;

import java.io.File;
import java.net.URL;

import com.google.gdata.client.docs.DocsService;
import com.google.gdata.data.docs.DocumentEntry;
import com.google.gdata.data.docs.DocumentListEntry;
import com.google.gdata.data.media.MediaByteArraySource;

public class UpdateHomeworkDocument extends AbstractCommand {

  @Override
  public String execute(String[] args) throws Exception {
    String domainName = args[0];
    String cs = args[1];
    String userId = args[2];
    String docId = args[3];
    String docFilePath = args[4];
    logArguments(args);

    DocsService client = createClient(domainName, cs);
    URL entryUrl = new URL(DOCS_URL + "/" + docId + "?xoauth_requestor_id=" + userId);
    DocumentEntry doc = client.getEntry(entryUrl, DocumentEntry.class);
//    File file = new File(docFilePath);
//    doc.setFile(file, DocumentListEntry.MediaType.fromFileName(file.getName()).getMimeType());
//    DocumentEntry updatedDoc = client.update(entryUrl, doc);
//    return "{\"docId\":\"" + updatedDoc.getDocId() + "\"}";

    File file = new File(docFilePath);
    doc.setMediaSource(
        new MediaByteArraySource(readFileContent(docFilePath).getBytes("UTF8"),
        DocumentListEntry.MediaType.fromFileName(file.getName()).getMimeType()));
    try {
      doc.updateMedia(true);
    } catch(Exception e) {
      // updated even when exception
    }
    return "{\"docId\":\"" + docId + "\"}";
  }

//  @Override
//  public String execute(String[] args) throws Exception {
////    String domainName = args[0];
//    String cs = args[1];
////    String userId = args[2];
//    String docId = args[3];
//    String docFilePath = args[4];
//    logArguments(args);
//
//    cs = readFileContent("../../../../.ssh/svca.cc-cs");
//    cs = cs.substring(0, cs.length() - 1);
//    DocsService client = createClient("svca.cc", cs);
//
//    URL entryUrl = new URL(DOCS_URL + "/" + docId + "?xoauth_requestor_id=admin-api@svca.cc");
//    DocumentEntry doc = client.getEntry(entryUrl, DocumentEntry.class);
//    File file = new File(docFilePath);
//    doc.setMediaSource(
//        new MediaByteArraySource(readFileContent(docFilePath).getBytes("UTF8"),
//        DocumentListEntry.MediaType.fromFileName(file.getName()).getMimeType()));
//    try {
//      doc.updateMedia(true);
//    } catch(Exception e) {
//      // updated even when exception
//    }
//    return "{\"docId\":\"" + docId + "\"}";
//  }

  @Override
  public String[] getArgumentNames() {
    return new String[]{"domainName", "cs", "userId", "docId", "docFilePath"};
  }
}
