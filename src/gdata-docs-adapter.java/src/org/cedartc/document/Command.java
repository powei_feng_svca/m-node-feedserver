package org.cedartc.document;

public interface Command {

  public String execute(String[] args) throws Exception;

  public String[] getArgumentNames();

  public String getName();

  public void showHelp();
}
