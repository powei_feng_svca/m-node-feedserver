package org.cedartc.document;

import java.io.File;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import com.google.gdata.client.docs.DocsService;
import com.google.gdata.data.PlainTextConstruct;
import com.google.gdata.data.acl.AclEntry;
import com.google.gdata.data.acl.AclFeed;
import com.google.gdata.data.acl.AclRole;
import com.google.gdata.data.acl.AclScope;
import com.google.gdata.data.docs.DocumentListEntry;
//import com.google.gdata.data.media.MediaFileSource;

public class CreateHomeworkDocument extends AbstractCommand {

  @Override
  public String[] getArgumentNames() {
    return new String[]{"domainName", "cs", "userId", "title", "templateFilePath", "taUserId",
    	"teacherGroupId"};
  }

  @Override
  public String execute(String[] args) throws Exception {
    String domainName = args[0];
    String cs = args[1];
    String userId = args[2];
    String title = args[3];
    String templateFilePath = args[4];
    String taUserId = args[5];
    String teacherGroupId = args[6];
    logArguments(args);

    DocsService client = createClient(domainName, cs);

    URL feedUri = new URL(DOCS_URL + "?xoauth_requestor_id=" + userId);
    DocumentListEntry newDocument = new DocumentListEntry();
    newDocument.setTitle(new PlainTextConstruct(title));
    File file = new File(templateFilePath);
    newDocument.setFile(
        file, DocumentListEntry.MediaType.fromFileName(file.getName()).getMimeType());
    newDocument = client.insert(feedUri, newDocument);

    // enable TA access
    AclEntry aclEntry = new AclEntry();
    aclEntry.setRole(AclRole.WRITER);
    aclEntry.setScope(new AclScope(AclScope.Type.USER, taUserId));
    client.insert(new URL(newDocument.getAclFeedLink().getHref()), aclEntry);

    // enable teacher access
    aclEntry = new AclEntry();
    aclEntry.setRole(AclRole.WRITER);
    aclEntry.setScope(new AclScope(AclScope.Type.GROUP, teacherGroupId));
    client.insert(new URL(newDocument.getAclFeedLink().getHref()), aclEntry);

    Map<String, Object> map = new HashMap<String, Object>();
    map.put("href", newDocument.getDocumentLink().getHref());
    return convertMapToJson(map);
  }

  public String execute_bugfix(String[] args) throws Exception {
    String userId = args[2];
    String title = args[3];
    String templateFilePath = args[4];
    String taUserId = args[5];
    String teacherGroupId = args[6];
    logArguments(args);

    String cs = readFileContent("svca.cc-cs");
    cs = cs.substring(0, cs.length() - 1);
    DocsService client = createClient("svca.cc", cs);

    URL feedUri = new URL(DOCS_URL + "?xoauth_requestor_id=jun.yang@svca.cc");
    DocumentListEntry newDocument = new DocumentListEntry();
    newDocument.setTitle(new PlainTextConstruct(title));
    File file = new File(templateFilePath);
    newDocument.setFile(
        file, DocumentListEntry.MediaType.fromFileName(file.getName()).getMimeType());
    newDocument = client.insert(feedUri, newDocument);

    URL aclFeedUrl = new URL(newDocument.getAclFeedLink().getHref());

    // transfer ownership to final anchor
    AclEntry aclEntry = new AclEntry();
    aclEntry.setRole(AclRole.OWNER);
    aclEntry.setScope(new AclScope(AclScope.Type.USER, "admin-api@svca.cc"));
    aclEntry = client.insert(aclFeedUrl, aclEntry);

    // delete temporary anchor
    for (AclEntry ae: client.getFeed(aclFeedUrl, AclFeed.class).getEntries()) {
      if (ae.getScope().getValue().equals("jun.yang@svca.cc")) {
        ae.delete();
        break;
      }
    }

    aclFeedUrl = new URL(aclFeedUrl.toString().replaceAll("jun.yang", "admin-api"));

    // enable student access
    aclEntry = new AclEntry();
    aclEntry.setRole(AclRole.WRITER);
    aclEntry.setScope(new AclScope(AclScope.Type.USER, userId));
    aclEntry = client.insert(aclFeedUrl, aclEntry);

    // enable TA access
    aclEntry = new AclEntry();
    aclEntry.setRole(AclRole.WRITER);
    aclEntry.setScope(new AclScope(AclScope.Type.USER, taUserId));
    client.insert(aclFeedUrl, aclEntry);

    // enable teacher access
    aclEntry = new AclEntry();
    aclEntry.setRole(AclRole.WRITER);
    aclEntry.setScope(new AclScope(AclScope.Type.GROUP, teacherGroupId));
    client.insert(aclFeedUrl, aclEntry);

    Map<String, Object> map = new HashMap<String, Object>();
    map.put("href", newDocument.getDocumentLink().getHref());
    return convertMapToJson(map);
  }
}
