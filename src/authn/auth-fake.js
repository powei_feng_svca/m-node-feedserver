/**
 * Usage: use setFakeUser() to set fake user to be returned in getUser().
 */

var common = require('./auth-common');
var logger = require('../logger').create(module);

var fakeUser = {};

function setFakeUser(fuser) {
  fakeUser = fuser;
  logger.log('user=',fakeUser);
};

function setViewer(request, response, next) {
  request.user = fakeUser;
  next();
};

function getUser(request, response) {
  return fakeUser;
};

function routes(app) {
  app.get('/', setViewer);
};

logger.log('WARNING: Using fake user in authentication. This server should not be left running for long.');

exports.getUser = getUser;
exports.setFakeUser = setFakeUser;
exports.routes = routes;
exports.makeHandlerAuthenticate = common.makeHandlerAuthenticate;
