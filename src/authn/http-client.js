var urlParser = require('url');

function butLast(s) {
  return s.substring(0, s.length - 1);
};

function send(method, url, headers, body, handleResponse) {
  headers = headers || {};

  var parsedUrl = urlParser.parse(url, true);
  headers.host = parsedUrl.hostname;

  var options = {
    hostname: parsedUrl.hostname,
    port: parsedUrl.port || parsedUrl.protocol == 'http:' ? 80 : 443,
    path: parsedUrl.pathname + (parsedUrl.search || ''),
    method: method,
    headers: headers
  };
  var request = require(butLast(parsedUrl.protocol)).request(options, function(response) {
    var chunks = [];
    response.setEncoding('utf8');
    response.on('data', function(chunk) {
      chunks.push(chunk);
    });
    response.on('end', function() {
      response.data = chunks.join('');
      handleResponse(response);
    });
  });
  request.on('error', function(error) {
    console.log('httpClient error=', error);
  });
  if (body) request.write(body);
  request.end();
};

exports.send = send;
