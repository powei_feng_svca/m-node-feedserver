/**
 * Authentication module using Google OAuth 2
 */

var common = require('auth-common');
var fs = require('fs');
var httpClient = require('http-client');
var querystring = require('querystring');
var url = require('url');
var util = require('util');

var TOKEN_EQ = 'token=';
var GET_USER_INFO = 'https://www.googleapis.com/oauth2/v1/userinfo?access_token=';
var ENV = global.INSTANCE || process.env.NODE_FEEDSERVER_ENV;
var COOKIE_DOMAIN = process.env.NODE_FEEDSERVER_COOKIE_DOMAIN || '.svca.cc';

function receiveAccessToken(request, response) {
  var params = querystring.parse(url.parse(request.url).query);
  if (params.token) {
    httpClient.send('GET', GET_USER_INFO + params.token, null, null, function(rp) {
      var userInfo = JSON.parse(rp.data);
      common.setUser(request, response, userInfo.email, null);
      response.writeHead(302, {Location: params['continue']});
      response.end('');
    });
  } else {
    response.writeHead(400, {'Content-Type': 'text/plain'});
    response.end('no token received');
  }
};

function routes(app) {
  fs.readFile(process.env.HOME + '/.ssh/sk' + (ENV ? '.' + ENV : ''), 'utf8', function(error, sk) {
    if (error) {
      console.log(error);
    } else {
      common.init({
        logout: {path: '/auth/logout'},
        receiveCode: {path: '/auth/code'},
        receiveToken: {path: '/auth/token'},
        cookieDomain: COOKIE_DOMAIN,
        secret: sk
      });
      common.routes(app);
      app.get(common.config.receiveToken.path, receiveAccessToken);
    }
  });
};

exports.getUser = common.getUser;
exports.routes = routes;
exports.makeHandlerAuthenticate = common.makeHandlerAuthenticate;
