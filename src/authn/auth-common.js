var Cookies = require('cookies');
var keygrip;
var logger = require('../logger').create(module);
var url = require('url');

var AUTH_COOKIE_PATH = '/';
var CONTEXT = '/';
var COOKIE_SEPARATOR = ';';
var COOKIE_USER = 'USER';
var SEVEN_DAYS = 7 * 24 * 60 * 60 * 1000;
var SIGNED = true;

var config = {};

function init(cfg) {
  for (var p in cfg) {
    config[p] = cfg[p];
  }
  keygrip = require('keygrip')([config.secret]);
};

/**
 * User: {id: 'john.doe@example.com', name: 'John Doe'}
 */
function setUser(request, response, id, name) {
  var cookies = new Cookies(request, response, keygrip);
  var user = {id: id, name: name};
  var options = {
    domain: config.cookieDomain,
    path: AUTH_COOKIE_PATH,
    expires: new Date(Date.now() + SEVEN_DAYS),
    httpOnly: true,
    signed: SIGNED
  };
  cookies.set(COOKIE_USER, JSON.stringify(user), options);
  logger.log('setUser() userCookie=' + JSON.stringify(user) + ' options=', options);
};

function getUser(request, response) {
  var user = null;
  try {
    var cookies = new Cookies(request, response, keygrip);
    var userCookie = cookies.get(COOKIE_USER, {signed: SIGNED});
    logger.log('getUser() userCookie=' + userCookie);
    if (userCookie) {
      user = JSON.parse(userCookie);
    }
  } catch(e) {
    console.error(e);
  }
  return user;
};

function logout(request, response) {
  var cookies = new Cookies(request, response, keygrip);
  cookies.set(COOKIE_USER, '', {signed: SIGNED, domain: config.cookieDomain, path: AUTH_COOKIE_PATH});
  response.writeHead(302, {'Location': CONTEXT});
  response.end('');
  console.log('logged out');
};

function routes(app) {
  app.get(config.logout.path, logout);
};

function makeHandlerAuthenticate(loginPath, excludedPaths) {
  var authn = this;
  return function(request, response, next) {
    function applicable(parsedUrl, excludedPaths) {
      for (var i = 0; i < excludedPaths.length; i++) {
        if (parsedUrl.pathname.indexOf(excludedPaths[i]) == 0) {
          // path starts with the excluding pattern
          return false;
        }
      }
      return true;
    };

    var parsedUrl = url.parse(request.url);
    if (applicable(parsedUrl, excludedPaths)) {
      var user = authn.getUser(request, response);
      if (user) {
        request.user = user;
        next(); // continue
      } else {
        var location = loginPath + '?' + parsedUrl.pathname;
        logger.log('No cookie; redirecting to ' + location);
        response.writeHead(302, {
            Location: location,
           'Set-Cookie': 'continuation=' + parsedUrl.pathname + '; Path=/'
        });
        response.end('');
      }
    } else {
      next();
    }
  };
};

exports.init = init;
exports.config = config;
exports.setUser = setUser;
exports.getUser = getUser;
exports.routes = routes;
exports.makeHandlerAuthenticate = makeHandlerAuthenticate;
