/*
 * Command line adapter.
 * Operation configuratoin:
 * "<nameOfOperation>": {
 *   "command": "command line with ${...} variables populated with parameters"
 * }
 * Example:
 * "ls": {
 *   "command": "ls ${options} ${directory}"
 * }
 */

var exec = require('child_process').exec;
var logger = require('logger').create(module);
var template = require('template');

function callOperation(params, feedConfig, dataSourceConfig, next) {
  var command = feedConfig.operations[params.__operationName].command;
  execute(command, params, function(result) {
    next(isError(result) ? result: formatResult(result));
  });
};

function execute(commandTemplate, params, next) {
  var command = template.fill(commandTemplate, params);
  exec(command, function(error, stdout, stderr) {
    if (error) {
      if (stderr) error.message = stderr;
      error.isError = true;
      next(error);
    } else if (stderr) {
      error = new Error(stderr);
      error.isError = true;
      next(error);
    } else {
      next(stdout);
    }
  });
};

function isError(e) {
  return e.toString() == 'Error';
};

function formatResult(result) {
  return {stdout: result};
};

exports.callOperation = callOperation;
