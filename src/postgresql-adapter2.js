/*
 * PostgreSQL adapter for use with https://github.com/brianc/node-postgres
 */

var logger = require('./logger').create(module);
var {Client} = require('pg');
var template = require('./template');
var util = require('util');

// expected number of rows in result set
var RESULT_SET_0 = 0;  // 0 expected
var RESULT_SET_1 = 1;  // 0 or 1 expected
var RESULT_SET_N = 2;  // 0 or more expected

var SQL_TYPE_VARCHAR = 'VARCHAR';
var SQL_TYPE_NUMERIC = 'NUMERIC';
var SQL_TYPE_BOOLEAN = 'BOOLEAN';
var SQL_TYPE_DATE = 'DATE';
var SQL_TYPE_TIME = 'TIME';

//pg.defaults.poolSize = 500;

function extend(object0, object1) {
  object0.__proto__ = object1;
  return object0;
};

async function executeSql(connectString, query, next, nRowsExpected) {
  logger.log('executeSql: connecting: ' + connectString);
  const client = new Client({
    connectionString: connectString,
  });
  try {
    const connectStart = new Date();
    await client.connect();
    logger.log('executeSql: connected (' + (new Date() - connectStart) + 
               'ms): ' + connectString);
    logger.log('executeSql: query started: ' + query);
    const queryStart = new Date();
    try {
      const results = await client.query(query);
      await client.query('commit');
      logger.log('executeSql: query ended (' +
                    (new Date() - queryStart) + 'ms): ' + query);
      next(adjustResultSet(results.rows, nRowsExpected));
    } catch (err) {
      await client.query('rollback');
      logger.log('executeSql: query failed: ' + err);
      handleSqlError(err, next);
    }
  } catch (err) {
    logger.log('executeSql: failed to connect: ' + err);
    handleSqlError(err, next);
  } finally {
    console.log('ending');
    client.end();
  }
};

function escapeSingleQuote(value) {
  return value && value.replace ? value.replace(/'/g, "''") : value;
};

/*
 * TODO: Let server handle SQL injection
 *
 * @param type {String}
 * @param value {String}
 * @return {String}
 */
function quoteSqlValue(type, value) {
  type = type || SQL_TYPE_VARCHAR;
  switch(type) {
    case SQL_TYPE_NUMERIC:
      if (value) {
        var numericValue = Number(value);
        if (numericValue == NaN) {
          throw new Error('Type ' + type + ' expected but got value of ' + value +
              '(type ' + typeof(value) + ')');
        } else {
          return numericValue;
        }
      } else {
        return 'null';
      }

    case SQL_TYPE_BOOLEAN:
      value = value ? value.toLowerCase() : value;
      if (value == 'true' || value == 'false' || value == null) {
        return value;
      } else {
        throw new Error('Type ' + type + ' expected but got value of ' + value +
            '(type ' + typeof(value) + ')');
      }

    // TODO: complete
    case SQL_TYPE_DATE:
    case SQL_TYPE_TIME:
    case SQL_TYPE_VARCHAR:
      return value ? '\'' + escapeSingleQuote(value) + '\'' : null;

    default:
      throw new Error('Unsupported type ' + type);
  }
};

function adjustResultSet(result, nRowsExpected) {
  switch(nRowsExpected) {
    case RESULT_SET_0:
      return '';

    case RESULT_SET_1:
      return result.length >= 1 ? result[0] : null;
   
    case RESULT_SET_N:
      return result;
  }
};

// TODO: Support max-results and start-index
// TODO: Support pagination
function list(params, feedConfig, dataSourceConfig, next) {
  var queryName = params.query || 'query';
  var query = feedConfig.operations.list[queryName];
  if (!query) {
    throw new Error('query named "' + queryName +
        '" missing in operation "list" in feedConfig ' +
        util.inspect(feedConfig));
  }
  executeSql(dataSourceConfig.connectString,
      template.fill(query, params, quoteSqlValue),
      next, RESULT_SET_N);
};

function get(params, feedConfig, dataSourceConfig, next) {
  executeSql(dataSourceConfig.connectString,
      template.fill(feedConfig.operations.get.query, params, quoteSqlValue),
      function(result) {
        next(result ? result : ERROR.NOT_FOUND);
      }, RESULT_SET_1);
};

function insert(params, entry, feedConfig, dataSourceConfig, next) {
  params = extend(entry, params);
  executeSql(dataSourceConfig.connectString,
      template.fill(feedConfig.operations.insert.query, params, quoteSqlValue) +
          ' returning lastval() as "entryId"',
      function(result) {
        notifyChange(feedConfig, dataSourceConfig, 'inserted');
        result && result.isError ?
            next(result) :
            get(extend(result, params), feedConfig, dataSourceConfig, next);
      }, RESULT_SET_1
  );
};

function update(params, entry, feedConfig, dataSourceConfig, next) {
  params = extend(entry, params);
  executeSql(dataSourceConfig.connectString,
      template.fill(feedConfig.operations.update.query, params, quoteSqlValue) +
          ' returning "' + getIdColumnName(feedConfig) + '" as "entryId"',
      function(result) {
        notifyChange(feedConfig, dataSourceConfig, 'updated');
        result && result.isError ?
            next(result) :
            get(extend(result, params), feedConfig, dataSourceConfig, next);
      }, RESULT_SET_1
  );
};

function remove(params, feedConfig, dataSourceConfig, next) {
  executeSql(dataSourceConfig.connectString,
      template.fill(feedConfig.operations.remove.query, params, quoteSqlValue) +
          ' returning "' + getIdColumnName(feedConfig) + '"',
      function(result) {
        notifyChange(feedConfig, dataSourceConfig, 'removed');
        next(result ? '' : ERROR.NOT_FOUND);
      }, RESULT_SET_1
  );
};

/**
 * Listen for change notifications sent to the "listentChannels".  As soon as
 * a notification is received, this pg client is terminated and thus API client
 * is responsible for calling listen() again if it needs to listen continuously.
 */
function listen(params, feedConfig, dataSourceConfig, next) {
  var listenChannels = feedConfig.listenChannels;
  if (listenChannels) {
    const client = new Client({
      connectionString: dataSourceConfig.connectString
    });
    client.connect(function(error) {
      if (error) {
//        client.end();
        logger.log('listen: failed to connect: ' + error);
        next(error);
      } else { 
        for (var i = 0; i < listenChannels.length; i++) {
          client.query('listen "' + listenChannels[i] + '"');
          logger.log('listen: ' + listenChannels[i]);
          client.on('notification', function(notification) {
//            client.end();
            if (isCaredChangeType(notification.payload, params.changeTypes)) {
              next(notification.payload, notification);
            }
          });
        }
      }
    });
  }
};

function getIdColumnName(feedConfig) {
  return feedConfig.idColumnName || 'id';
};

function notifyChange(feedConfig, dataSourceConfig, changeType) {
  if (feedConfig.notifyChannel) {
    executeSql(dataSourceConfig.connectString,
        'notify "' + feedConfig.notifyChannel + '", \'' + changeType + '\'', function(){});
  };
};

function isCaredChangeType(changeType, caredChangeTypes) {
  if (caredChangeTypes) {
    for (var i = 0; i < caredChangeTypes.length; i++) {
      if (changeType == caredChangeTypes[i]) {
        return true;
      }
    }
    return false;
  } else {
    // none specified; assumed to care about all
    return true;
  }
};

function handleSqlError(error, next) {
  error.isError = true;
  next(error);
};

function createError(message) {
  var error = new Error(message);
  error.isError = true;
  error.stack = null;
  return error;
};

var ERROR = {
  NOT_FOUND: createError('not found')
};

exports.list = list;
exports.get = get;
exports.insert = insert;
exports.update = update;
exports.remove = remove;
exports.listen = listen;
exports.ERROR = ERROR;
