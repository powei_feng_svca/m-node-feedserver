// generates browser API client

var api = require('./client').generate();

function generate(namespace) {
  var code = ['var api = {' + namespace + ': {\n'];
  for (var feedName in api[namespace]) {
    code.push('  ' + feedName + ':\n');
    var feed = api[namespace][feedName];
    code.push('  {\n');
    for (var operationName in feed) {
      var operation = feed[operationName];
      code.push('    ' + operationName + ': function(args, body, handleResult, handleError) {\n');
      code.push('      var ns = "' + operation.ns + '";\n');
      code.push('      var fc = "' + operation.fn + '";\n');
      code.push('      var on = "' + operation.on + '";\n');
      code.push('      (' + operation.toString() + '\n');
      code.push('      )(args, body, handleResult, handleError);\n');
      code.push('    },\n');
    }
    code.push('  },\n');
  };
  code.push('}, $: {handleError: function(e){console.log(e)}}}\n');

  console.log(code.join(''));
};

if (process.argv.length != 3) {
  console.log('usage:', process.argv[0], process.argv[1], '<namespace>');
} else {
  generate(process.argv[2]);
}
