PORT = process.env.NODE_FEEDSERVER_PORT || 8002;
EXT_PREFIX = process.env.EXT_PREFIX || 'http://student2-cn.cedartc.org/api';
IS_DEV = process.env.NODE_FEEDSERVER_ENV == 'DEV';

var api = require('./feed-provider');
var logger = require('morgan');
var express = require('express');
var guard = require('./guard');
var signedFetch = require('signed-fetch');

var app = express();
app.use(logger(':method :url :status :response-time'));
app.use(signedFetch.verify(EXT_PREFIX, 'igoogle.pem'));
api.routes(null, false)(app);
app.listen(PORT);
console.log('Serving listening on port *:' + PORT);
