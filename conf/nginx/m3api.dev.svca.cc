server {
  server_name m3api.dev.svca.cc;
  access_log /home/pfeng/log/corstest.dev.svca.cc.access.log;
  error_log /home/pfeng/log/corstest.dev.svca.cc.error.log info;

  # document root
  location / {
    add_header 'Access-Control-Allow-Origin' '*';
    add_header 'Access-Control-Allow-Methods' 'GET, POST, OPTIONS';
    add_header 'Access-Control-Expose-Headers' '*';
    proxy_set_header X-Forwarded-Host $host;
    proxy_set_header X-Forwarded-Server $host;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_pass http://localhost:1984;
  }


    listen 443 ssl; # managed by Certbot
    ssl_certificate /etc/letsencrypt/live/m3api.dev.svca.cc/fullchain.pem; # managed by Certbot
    ssl_certificate_key /etc/letsencrypt/live/m3api.dev.svca.cc/privkey.pem; # managed by Certbot
    include /etc/letsencrypt/options-ssl-nginx.conf; # managed by Certbot
    ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem; # managed by Certbot

}
server {
    if ($host = m3api.dev.svca.cc) {
        return 301 https://$host$request_uri;
    } # managed by Certbot


  server_name m3api.dev.svca.cc;
    listen 80;
    return 404; # managed by Certbot


}
